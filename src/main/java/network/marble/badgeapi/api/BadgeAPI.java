package network.marble.badgeapi.api;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import network.marble.badgeapi.BadgeAPIPlugin;
import network.marble.datastoragemanager.api.DataAPI;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.Badge;
import network.marble.datastoragemanager.database.models.NetworkUser;
import network.marble.datastoragemanager.database.models.NetworkUserBadges;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

public class BadgeAPI {
	public static void setPlayerBadge(UUID user, String plugin, String badge, int progress){
		if (progress > 0){
			NetworkUser netUser = DataAPI.getUser(user.toString());
			Badge gottenBadge = getBadge(plugin, badge);
			
			List<NetworkUserBadges> badges = netUser.getNetworkUserBadges();
			if (badges != null && !badges.isEmpty()){
				boolean exists = false;
				for (NetworkUserBadges b : badges){
					
					if (b.getBadge().getParentPlugin().equals(plugin) && b.getBadge().getName().equals(badge)){
						Integer existingProgress = b.getProgress();
						Integer progressLimit = gottenBadge.getProgressLimit();
						if (existingProgress >= progressLimit){
							return;
						} else if (progress > existingProgress){
							exists = true;
							b.setProgress(progress);
							netUser.save();
						}
					}
				}
				
				if(!exists){
					NetworkUserBadges nwb = new NetworkUserBadges(progress, gottenBadge);
					netUser.getNetworkUserBadges().add(nwb);
				}
				
			}else{
				NetworkUserBadges nwb = new NetworkUserBadges(progress, gottenBadge);
				netUser.getNetworkUserBadges().add(nwb);
			}

			if (gottenBadge != null){//null check wont work
				DAOManager.getNetworkUserDAO().save(netUser);
				if (gottenBadge.getProgressLimit() == progress){
					Player p = Bukkit.getPlayer(user);
					p.sendMessage(ChatColor.GOLD + "You earned the badge: " + ChatColor.GREEN + badge);
					launchFireworkRing(p);
				}
			} else {
				BadgeAPIPlugin.getPlugin().getLogger().info("badge made");
			}
		}
	}

	/***
	 * @return Returns an integer count of all non-limited badges.
	 */
	public static int getAmountOfExistingBadges(){
		return DAOManager.getBadgeDAO().createQuery().filter("isLimited =", false).asList().size();
	}

	public static int getPlayerBadgeProgress(UUID user, String plugin, String badge){
		NetworkUser netUser = DataAPI.getUser(user.toString());

		List<NetworkUserBadges> badges = netUser.getNetworkUserBadges();
		if (badges != null){
			for (NetworkUserBadges b : badges){
				if (b.getBadge().getParentPlugin().equals(plugin) && b.getBadge().getName().equals(badge)){
					return b.getProgress();
				}
			}
		}

		return 0;
	}

	public static List<NetworkUserBadges> getAllPlayerBadgesInProgress(UUID user){
		NetworkUser u = DataAPI.retrieveUser(user.toString());
		List<NetworkUserBadges> badges = u.getNetworkUserBadges();
		List<NetworkUserBadges> inProgress = new ArrayList<NetworkUserBadges>();

		if (badges != null){
			for (NetworkUserBadges b : badges){
				if (b.getProgress() != b.getBadge().getProgressLimit()){
					inProgress.add(b);
				}
			}
		}

		return inProgress;
	}

	public static List<NetworkUserBadges> getAllPlayerBadgesCompleted(UUID user){
		NetworkUser u = DataAPI.retrieveUser(user.toString());
		List<NetworkUserBadges> badges = u.getNetworkUserBadges();
		List<NetworkUserBadges> completed = new ArrayList<NetworkUserBadges>();

		if (badges != null){
			for (NetworkUserBadges b : badges){
				if (b.getProgress() == b.getBadge().getProgressLimit()){
					completed.add(b);
				}
			}
		} else {
			BadgeAPIPlugin.getPlugin().getLogger().info("Null in getAllPlayerBadgesCompleted");

			return new ArrayList<NetworkUserBadges>();
		}
		return completed;
	}

	public static void createBadgeIfNotExists(String plugin, String badgeName, String description, int progressLimit, boolean givesBackgroundArt, boolean isLimited){
		Badge searchedBadge = getBadge(plugin, badgeName);
		if (searchedBadge == null){
			Badge badge = new Badge(plugin, badgeName, description, progressLimit, new ArrayList<String>(),
					givesBackgroundArt, isLimited);
			BadgeAPIPlugin.getPlugin().getLogger().info("badge made");
			badge.save();
		}
	}

	public static Badge getBadge(String plugin, String badgeName){
		return Badge.load(badgeName, plugin);
	}
	
	public static void launchFireworkRing(Player p){
		Location location = p.getLocation();
		for (int i = 0; i < 360; i += 15){
			Color c;
			if(i % 30 == 0){
				c = Color.RED;
			}else{
				c = Color.GREEN;
			}
			double angle = i * Math.PI / 180;
			double radius = 2.0;
			double x = location.getX() + radius * Math.cos(angle);
			double z = location.getZ() + radius * Math.sin(angle);
			Location newLoaction = new Location(p.getWorld(), x, location.getY(), z);
			Firework fw = (Firework) p.getWorld().spawn(newLoaction, Firework.class);
			
			double distanceX = location.getX() - x;
			double distanceZ = location.getZ() - z;
			double xDecimal = distanceX / radius;
			double zDecimal = distanceZ / radius;
			
			fw.setVelocity(new Vector((xDecimal*0.1)/(radius*2), 0.01, (zDecimal*0.1)/(radius*2)));
			//fw.setVelocity(new Vector(0, 0.01, 0));
			
			FireworkMeta fwm = fw.getFireworkMeta();
			fwm.addEffect(FireworkEffect.builder().flicker(true).withColor(c).trail(true).build());
			fwm.setPower(2);
			fw.setFireworkMeta(fwm);
			//TODO angle rockets
		}
	}
}
