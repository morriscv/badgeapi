package network.marble.badgeapi.api;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import network.marble.inventoryapi.api.InventoryAPI;
import network.marble.inventoryapi.itemstacks.ActionItemStack;

public class BadgeMenus {
	public static void buildInventoryMenus() {
    	ItemStack badgeProgressIS = new ItemStack(Material.PAINTING, 1, (short)0);
    	ItemMeta badgeProgressMeta = badgeProgressIS.getItemMeta();
    	badgeProgressMeta.setDisplayName(ChatColor.GOLD + "Badges");
    	badgeProgressIS.setItemMeta(badgeProgressMeta);
    	
    	ActionItemStack soonAchievement = new ActionItemStack(badgeProgressIS, "badges earned", false);
    	InventoryAPI.addGlobalInventoryItem(soonAchievement, 5, 1);
	}
}
