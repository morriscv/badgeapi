package network.marble.badgeapi.commands;

import net.md_5.bungee.api.ChatColor;
import network.marble.badgeapi.BadgeAPIPlugin;
import network.marble.badgeapi.api.BadgeAPI;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import network.marble.datastoragemanager.database.models.NetworkUserBadges;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BadgeCommands implements CommandExecutor{
	
	public BadgeCommands() {}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		boolean commandSuccess = false;
		if (cmd.getName().equalsIgnoreCase("badges")) {
			if (sender instanceof Player) {
				if(args.length > 0 && args.length <= 2){
					Player player = (Player) sender;
					String subCmd = args[0].toLowerCase();
					commandSuccess = true;
					switch(subCmd){
						case "earned": earnedBadges(player); break;
		           		default: sender.sendMessage(ChatColor.RED + "Invalid action."); commandSuccess = false; break;
					}
				}
			}else{
	           sender.sendMessage("You must be a player to get badges!");
	           return false;
			}
		}
		return commandSuccess;
	}
	
	private void earnedBadges(Player p){
		List<NetworkUserBadges> playerBadges = BadgeAPI.getAllPlayerBadgesCompleted(p.getUniqueId());

		double percentage = 0;
		if(!playerBadges.isEmpty()){
			percentage = ((double)playerBadges.size() / (double)BadgeAPI.getAmountOfExistingBadges()) * 100;
		}
		
		if(percentage > 0){
			String percentageString = "";
			if((percentage % 1) > 0){
				DecimalFormat df = new DecimalFormat("#.##");
				df.setRoundingMode(RoundingMode.FLOOR);
				percentageString += ChatColor.GREEN + (df.format(percentage) + "%");
			}else{
				percentageString += ChatColor.GREEN + ((int)percentage + "%");
			}
			
			p.sendMessage(ChatColor.GOLD + "You have earned " + percentageString + ChatColor.GOLD + " of the available badges!");
			p.sendMessage(ChatColor.GOLD + "Your last earned badge: " + ChatColor.GREEN + playerBadges.get(playerBadges.size()-1).getBadge().getName());
			//Get closest badges
			
			
			List<NetworkUserBadges> inProgress = BadgeAPI.getAllPlayerBadgesInProgress(p.getUniqueId());
			
			if(!inProgress.isEmpty()){
				List<NetworkUserBadges> closest = new ArrayList<NetworkUserBadges>();
				
				for(int i = 0; i < inProgress.size() && i < 7; i++){
					double progress = -1;
					NetworkUserBadges nextBadge = null;
					for(NetworkUserBadges badge : inProgress){
						double thisProgress = (double)badge.getProgress() / (double)badge.getBadge().getProgressLimit();
						Bukkit.getLogger().info(badge.getBadge().getName());
						if(thisProgress > progress){
							progress = thisProgress;
							nextBadge = badge;
						}
					}
					
					closest.add(nextBadge);
					inProgress.remove(nextBadge);
				}
				
				
				if(!closest.isEmpty()){
					p.sendMessage(ChatColor.GOLD + "So close:");
				
					for(NetworkUserBadges near: closest){
						int pertenthForCurrentBadge = (int) (((double)near.getProgress() / (double)near.getBadge().getProgressLimit()) * 10);
						String progressBar = ChatColor.GOLD + "[" + ChatColor.GREEN + new String(new char[pertenthForCurrentBadge]).replace("\0", "#") + ChatColor.GOLD + new String(new char[10 - pertenthForCurrentBadge]).replace("\0", "_") + "]";
						p.sendMessage(progressBar + " - " + near.getBadge().getName());
					}
				}
			}
		}else{
			p.sendMessage(ChatColor.GOLD + "Your have not earned any badges, let's fix that!");
			BadgeAPI.setPlayerBadge(p.getUniqueId(), BadgeAPIPlugin.getPlugin().getName(), "Progress Checker", 1);
			earnedBadges(p);
			return;
		}
		
		BadgeAPI.setPlayerBadge(p.getUniqueId(), BadgeAPIPlugin.getPlugin().getName(), "Progress Checker", 1);
	}
	
}
