package network.marble.badgeapi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import network.marble.badgeapi.api.BadgeAPI;
import network.marble.badgeapi.api.BadgeMenus;
import network.marble.badgeapi.commands.BadgeCommands;

public class BadgeAPIPlugin extends JavaPlugin {
    private static BadgeAPIPlugin plugin;
    
    @Override
    public void onEnable() {
    	if(isDataManagerPresent()){
	        plugin = this;
	        registerCommands();
	        BadgeMenus.buildInventoryMenus();
	        registerBadges();
	        getLogger().info("BadgeAPI successfully loaded.");
    	}else{
    		getLogger().severe("Data Manager is not present. Disabling.");
    		setEnabled(false);
    	}
    }
    
    private void registerBadges() {
		BadgeAPI.createBadgeIfNotExists(getName(), "Progress Checker", "Awarded for checking your badge progress.", 1, false, false);
		BadgeAPI.createBadgeIfNotExists(getName(), "Secret", "Awarded for sneak-beakery", 1, false, false);
		BadgeAPI.createBadgeIfNotExists(getName(), "Third", "3", 1, false, false);
	}

	private void registerCommands() {
    	getCommand("badges").setExecutor(new BadgeCommands());
    }
    
    public static BadgeAPIPlugin getPlugin() {
        return plugin;
    }

    public static boolean isDataManagerPresent(){ 
        return Bukkit.getPluginManager().getPlugin("MRN-DataStorageManager") != null;
    } //Return true if the DataStorageManager is found, and therefore it is safe to use functionality from it, or false otherwise.
    
}
